package edu.byu.hbll.emailutils;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.mail.*;
import javax.mail.internet.MimeMessage;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class tests the functionality of the EmailUtils class.
 * 
 * @author tnelsonw
 *
 */
public class EmailUtilsIT {

  private static final Logger logger = LoggerFactory.getLogger(EmailUtilsIT.class);

  private static EmailUtils utils;

  /**
   * Sets up the environment for the tests to run in by reading from the test/resources/config.yml
   * file.
   */
  @BeforeClass
  public static void setUpClass() {

    EmailConfig config = new EmailConfig();
    Path[] paths = new Path[1];
    paths[0] = Paths.get("src/test/resources/config.yml");
    try {
      config.loadConfigFrom(paths);
    } catch (IOException e) {
      logger.error(e.getMessage());
    }
    utils = config.getEmailUtils();
    utils.setSubject(config.getSubject());
    utils.setFromWhom(config.getFromWhom());
    utils.setToWhom(config.getToWhom());
    utils.setMessageText(config.getMessageText());
  }

  /**
   * This method connects to the senders email and looks for the message in the senders inbox. To
   * verify that the send function is properly functioning, add yourself as a recipient in the
   * "toWhom" field of the config.yml file.
   *
   * @return a TestMessage object to use in the EmailUtilsIT class
   * @throws MessagingException if checking the inbox fails to connect
   * @throws IOException if checking the inbox fails to read
   */
  private TestMessage checkMostRecentInboxMessage() throws MessagingException, IOException {

    Properties properties = new Properties();
    properties.setProperty("mail.store.protocol", utils.getProtocol());

    Session session = Session.getDefaultInstance(properties, null);
    Store store = session.getStore(utils.getProtocol());
    store.connect(utils.getHost(), utils.getUsername(), utils.getPassword());

    Folder root = store.getDefaultFolder();
    Folder inbox = root.getFolder("inbox");
    inbox.open(Folder.READ_WRITE);
    Message[] msgs = inbox.getMessages();
    if (msgs.length == 0) {
      logger.info("No messages in inbox");
    }

    MimeMessage msg = (MimeMessage) msgs[msgs.length - 1];
    TestMessage tm =
            new TestMessage(msg.getContentType(), msg.getSubject(), msg.getContent().toString());

    inbox.close(true);
    store.close();
    return tm;
  }

  /**
   * This method tests the send functionality of the EmailUtils class by using the
   * checkMostRecentInboxMessage function.
   * 
   * @throws MessagingException if checking the inbox fails to connect
   * @throws IOException if checking the inbox fails to read
   */
  @Test
  public void testSend() throws MessagingException, IOException, InterruptedException {
    MimeMessage m = (MimeMessage) utils.send();

    // wait a second to make sure the message is properly sent and received
    TimeUnit.SECONDS.sleep(2);
    TestMessage msg = checkMostRecentInboxMessage();

    TestMessage tm =
        new TestMessage(m.getContentType(), m.getSubject(), m.getContent().toString());

    assertTrue(tm.equals(msg));
  }

}
