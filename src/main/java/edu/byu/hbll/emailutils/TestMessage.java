package edu.byu.hbll.emailutils;

import java.util.Objects;

/**
 * This is a simple class to test whether two messages are the same.
 * 
 * @author tnelsonw
 *
 */
public class TestMessage {

  private String contentType;

  private String subject;

  private String content;

  /**
   * Constructor for this class.
   * 
   * @param contentType The content type of the message
   * @param subject The message subject.
   * @param content The actual written content of the message.
   */
  public TestMessage(String contentType, String subject, String content) {
    this.contentType = contentType;
    this.subject = subject;
    this.content = content.trim();
  }

  /**
   * Simple getter method.
   * 
   * @return the subject of the message
   */
  public String getSubject() {
    return subject;
  }

  /**
   * Simple getter method.
   * 
   * @return the content type of the message
   */
  public String getContentType() {
    return contentType;
  }

  /**
   * Simple getter method.
   * 
   * @return the actual content of the message
   */
  public String getContent() {
    return content;
  }

  /**
   * Overridden hashCode method for this class type.
   *
   * @return the hash of the object.
   */
  @Override
  public int hashCode() {
    return Objects.hash(contentType, subject, content);
  }

  /**
   * Equals method to compare two TestMessage objects.
   * 
   * @param o The object to compare to.
   */
  @Override
  public boolean equals(Object o) {
    if (this.getClass() != o.getClass()) {
      return false;
    }
    if (this == o) {
      return true;
    }

    TestMessage tm = (TestMessage) o;
    if (this.contentType.equals(tm.getContentType())
            && this.subject.equals(tm.getSubject())
            && this.content.equals(tm.getContent())) {
          return true;
    }
    return false;
  }

}
